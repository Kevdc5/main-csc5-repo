/* 
 * File:   main.cpp
 * Author: kevinjaramillo
 *
 * Created on March 3, 2015, 11:09 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "****************************" <<endl;
    cout << 
            " CCC     SSSS      !!\n"
            "C   C   S    S     !!\n"
            "C        S         !! \n"
            "C          S       !!\n"
            "C    C   S    S    !!\n"
            " CCC       SSSS    00\n";
    cout << "*****************************" << endl;
    cout << "Computer Science is Cool Stuff";       
    
    return 0;
}

