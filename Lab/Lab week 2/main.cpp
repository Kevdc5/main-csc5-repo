/* 
 * File:   main.cpp
 * Author: kevinjaramillo
 *
 * Created on March 3, 2015, 11:34 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl;
    string userInput;
    cin >> userInput;
    cout << "Hello, ";
    cout << userInput;
    cout << ". I am glad to meet you.";
            
    return 0;
}

