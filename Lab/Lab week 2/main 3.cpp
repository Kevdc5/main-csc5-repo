/* 
 * File:   main.cpp
 * Author: kevinjaramillo
 *
 * Created on March 2, 2015, 7:44 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    double meter, feet, mile, inches ;
    cout << "Enter value in meters to be converted to other measurements" <<endl;
    cin >> meter;
    
    feet = meter * 3.2808;
    mile = meter / 1609.344;
   inches = meter * 39.37;
   
   cout << meter << " meters equal to " << feet << " feet." << endl;
    cout << meter << " meters equal to " << mile << " miles." << endl;
    cout << meter << " meters equal to " << inches << " inches." << endl;
    
    return 0;
}

