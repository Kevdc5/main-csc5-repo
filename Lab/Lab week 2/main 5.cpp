/* 
 * File:   main.cpp
 * Author: kevinjaramillo
 *
 * Created on March 3, 2015, 2:32 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int test1, test2, test3;
    
    cout << "Enter exam scores" << endl;
    cin >> test1;
    
    cout << "Enter exam 2 test score" << endl;
    cin >> test2;
    
    cout << "enter exam 3 test score" << endl;
    cin >> test3;
    
    cout << "average of the 3 exams " << (float)(test1+test2+test3) / 3 << endl;
            
    return 0;
}

